<div align="center">

<h1> <a href="https://qiplex.com/software/smart-file-renamer/">Smart File Renamer</a> </h1>
  
<h3> Find large files easily on Windows, macOS and Linux! </h3>

You can get the app on 
<a href="https://gitlab.com/Qiplex-Apps/smart-file-renamer/-/releases">GitLab</a>
or on 
<a href="https://qiplex.com/software/smart-file-renamer/">my website</a>
<br>Code comes soon.

![Smart File Renamer](https://qiplex.com/assets/img/app/main/smart-file-renamer-app.png)

<h4>Check out the app features below: </h4>
  
![Smart File Renamer - Features](https://user-images.githubusercontent.com/32670415/147224304-5b99ca20-bdae-47cd-b51a-52ed00c888d4.png)
  
</div>
